<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\CommonController;
use App\Http\Controllers\Common\SystemCotroller;
use App\Http\Controllers\Core\CoreController;

class FreshController extends Controller
{

    private $core;
    private $webset;
    private $system;
    private $common;

    #初始化采集核心
    public function __construct()
    {
        $this->core = new CoreController();
        #初始化设置项
        $this->webset = config('webset');
        #初始化系统参数
        $this->system = new SystemCotroller();
        #初始化公共控制器
        $this->common = new CommonController();

    }
}
